# tdx_strategy

#### 介绍
通达信策略网上整理的内容，不做商业用途，学习使用,收集网络上面优秀的通达信公式，避免以后没有学习内容

#### 软件架构
通达信策略网是一个提供股票交易策略、技术分析、投资理念等内容的平台。它为投资者提供了一个学习和交流的环境，帮助投资者提高投资技能，更好地进行股票交易。

在通达信策略网上，你可以找到各种类型的投资策略，包括但不限于短线交易策略、中长线投资策略、技术分析策略、基本面分析策略等。这些策略都是由经验丰富的投资者或专业的投资顾问提供的，具有很高的参考价值。

此外，通达信策略网还提供了丰富的学习资源，如投资教程、研究报告、市场分析等，帮助投资者深入理解股票市场的运行规律，提高投资决策的准确性。

总的来说，通达信策略网是一个非常适合投资者学习和交流的平台。无论你是新手还是老手，都能在这里找到适合自己的投资策略和学习资源。然而，投资者在使用这些策略时，也需要注意结合自身的投资目标和风险承受能力，做出最适合自己的投资决策。


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

### 1、顺势黑马主图指标：

【顺势黑马】套装指标简介：
这是一个依托EXPMA基础上添加五线通道，并实时显示五线通道的价格，来判断K线所在位置的参考。
1、“黄金金叉“显示阳线为黄，红色做多线持有；
2、“死叉阴线”为蓝；绿线做空线休息；
3、出信号放量突破时，均线多头排列可取，空排不取。
源代码

```
ABC7:=EMA(C,7),COLORYELLOW,LINETHICK2;
ABC14:=EMA(C,14),COLOR7FF00F,LINETHICK1 DOTLINE;
ABC25:=EMA(C,25),COLORFF7F00,LINETHICK1 DOTLINE;
ABCMA45:=EMA(C,45),COLORF00FFF,LINETHICK1 DOTLINE;
MA5:=MA(C,5);{微信公众号:尊重市场}
MA10:=MA(C,10);
MA20:=MA(C,20);
ABC:=ABC7>ABC14;
STICKLINE(C/REF(C,1)>1.095,C,O,2,0),COLORYELLOW;
DRAWTEXT(C/REF(C,1)>1.095,L*0.96,' ★强'),COLORLIRED;
STICKLINE(HIGH<REF(LOW,0),HIGH,REF(LOW,0),10,0);
STICKLINE(LOW>REF(HIGH,0) ,LOW,REF(HIGH,0),10,0);
STICKLINE(C=O,H,L,0,0);
STICKLINE((C=O)AND(C>REF(C,0)),C,O,8,0);
STICKLINE((C=O)AND(C<REF(C,0)),C,O,8,0);
STICKLINE(CROSS(ABC7,ABC14) AND ABC,CLOSE,OPEN,2,0),COLORMAGENTA;
DRAWICON(CROSS(ABC7,ABC14) AND ABC,L*1.002,9);
DRAWTEXT(CROSS(ABC7,ABC14) AND{微信公众号:尊重市场}ABC,L*0.98,' ★买'),COLORMAGENTA;
STICKLINE(CROSS(ABC25,ABC7),CLOSE,OPEN,2,0),COLORBLUE;
CC:=ABS((2*CLOSE+HIGH+LOW)/4-MA(CLOSE,20))/MA(CLOSE,20);
DD:=DMA(CLOSE,CC);
上轨:(1+7/100)*DD,DOTLINE,COLORGREEN;
下轨:(1-7/100)*DD,DOTLINE,COLORGREEN;
中轨:(上轨+下轨)/2,DOTLINE,COLORGREEN;
FK:(1+14/100)*DD,DOTLINE,COLORGRAY;
CD:(1-14/100)*DD,DOTLINE,COLORGRAY;
DRAWNUMBER(ISLASTBAR,上轨,上轨),COLOR00FFFF;
DRAWNUMBER(ISLASTBAR,下轨,下轨),COLORFFFF00;
DRAWNUMBER(ISLASTBAR,中轨,中轨),COLOR00FF00;
DRAWNUMBER(ISLASTBAR,FK,FK){微信公众号:尊重市场},COLOR0000FF;
DRAWNUMBER(ISLASTBAR,CD,CD),COLORWHITE;
上轨绿:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
上轨红:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
中轨绿:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
中轨红:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE COLORRED, LINETHICK1;
下轨绿:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
下轨红:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
IF(ABC7>REF(ABC7,1),ABC7,DRAWNULL),COLORRED,LINETHICK2;
IF(ABC7<REF(ABC7,1),ABC7,DRAWNULL),COLORGREEN,LINETHICK2;
```
综合交易模型python翻译的

```
from trader_tool.tdx_indicator import *
import pandas as pd
import akshare as ak
def take_advantage_of_the_trend(df):
    '''
    【顺势黑马】套装指标简介：
    这是一个依托EXPMA基础上添加五线通道，并实时显示五线通道的价格，来判断K线所在位置的参考。
    1、“黄金金叉“显示阳线为黄，红色做多线持有；
    2、“死叉阴线”为蓝；绿线做空线休息；
    3、出信号放量突破时，均线多头排列可取，空排不取。
    ABC7:=EMA(C,7),COLORYELLOW,LINETHICK2;
    ABC14:=EMA(C,14),COLOR7FF00F,LINETHICK1 DOTLINE;
    ABC25:=EMA(C,25),COLORFF7F00,LINETHICK1 DOTLINE;
    ABCMA45:=EMA(C,45),COLORF00FFF,LINETHICK1 DOTLINE;
    MA5:=MA(C,5);{微信公众号:尊重市场}
    MA10:=MA(C,10);
    MA20:=MA(C,20);
    ABC:=ABC7>ABC14;
    STICKLINE(C/REF(C,1)>1.095,C,O,2,0),COLORYELLOW;
    DRAWTEXT(C/REF(C,1)>1.095,L*0.96,' ★强'),COLORLIRED;
    STICKLINE(HIGH<REF(LOW,0),HIGH,REF(LOW,0),10,0);
    STICKLINE(LOW>REF(HIGH,0) ,LOW,REF(HIGH,0),10,0);
    STICKLINE(C=O,H,L,0,0);
    STICKLINE((C=O)AND(C>REF(C,0)),C,O,8,0);
    STICKLINE((C=O)AND(C<REF(C,0)),C,O,8,0);
    STICKLINE(CROSS(ABC7,ABC14) AND ABC,CLOSE,OPEN,2,0),COLORMAGENTA;
    DRAWICON(CROSS(ABC7,ABC14) AND ABC,L*1.002,9);
    DRAWTEXT(CROSS(ABC7,ABC14) AND{微信公众号:尊重市场}ABC,L*0.98,' ★买'),COLORMAGENTA;
    STICKLINE(CROSS(ABC25,ABC7),CLOSE,OPEN,2,0),COLORBLUE;
    CC:=ABS((2*CLOSE+HIGH+LOW)/4-MA(CLOSE,20))/MA(CLOSE,20);
    DD:=DMA(CLOSE,CC);
    上轨:(1+7/100)*DD,DOTLINE,COLORGREEN;
    下轨:(1-7/100)*DD,DOTLINE,COLORGREEN;
    中轨:(上轨+下轨)/2,DOTLINE,COLORGREEN;
    FK:(1+14/100)*DD,DOTLINE,COLORGRAY;
    CD:(1-14/100)*DD,DOTLINE,COLORGRAY;
    DRAWNUMBER(ISLASTBAR,上轨,上轨),COLOR00FFFF;
    DRAWNUMBER(ISLASTBAR,下轨,下轨),COLORFFFF00;
    DRAWNUMBER(ISLASTBAR,中轨,中轨),COLOR00FF00;
    DRAWNUMBER(ISLASTBAR,FK,FK){微信公众号:尊重市场},COLOR0000FF;
    DRAWNUMBER(ISLASTBAR,CD,CD),COLORWHITE;
    上轨绿:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    上轨红:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
    中轨绿:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    中轨红:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE COLORRED, LINETHICK1;
    下轨绿:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    下轨红:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
    IF(ABC7>REF(ABC7,1),ABC7,DRAWNULL),COLORRED,LINETHICK2;
    IF(ABC7<REF(ABC7,1),ABC7,DRAWNULL),COLORGREEN,LINETHICK2;
    翻译

    '''
    data=pd.DataFrame()
    data['date']=df['date']
    C=df['close']
    #ABC7赋值:收盘价的7日指数移动平均,画黄色,线宽为2
    ABC7=EMA(C,7)#,COLORYELLOW,LINETHICK2;
    #ABC14赋值:收盘价的14日指数移动平均,COLOR7FF00F,线宽为1 DOTLINE
    ABC14=EMA(C,14)#COLOR7FF00F,LINETHICK1 DOTLINE;
    #ABC25赋值:收盘价的25日指数移动平均,COLORFF7F00,线宽为1 DOTLINE
    ABC25=EMA(C,25)#,COLORFF7F00,LINETHICK1 DOTLINE;
    #ABCMA45赋值:收盘价的45日指数移动平均,COLORF00FFF,线宽为1 DOTLINE
    ABCMA45=EMA(C,45)#,COLORF00FFF,LINETHICK1 DOTLINE;
    MA5=MA(C,5)
    MA10=MA(C,10)
    MA20=MA(C,20)
    ABC=ABC7>ABC14
    #当满足条件收盘价/1日前的收盘价>1.095时,在收盘价和开盘价位置之间画柱状线,宽度为2,0不为0则画空心柱.,画黄色
    #STICKLINE(C/REF(C,1)>1.095,C,O,2,0),COLORYELLOW;
    #当满足条件收盘价/1日前的收盘价>1.095时,在最低价*0.96位置书写文字,画淡红色
    data['强']=C/REF(C,1)>1.09
    #DRAWTEXT(C/REF(C,1)>1.095,L*0.96,' ★强'),COLORLIRED;

    #当满足条件最高价<0日前的最低价时,在最高价和0日前的最低价位置之间画柱状线,宽度为10,0不为0则画空心柱.
    #STICKLINE(HIGH<REF(LOW,0),HIGH,REF(LOW,0),10,0);
    #STICKLINE(LOW>REF(HIGH,0) ,LOW,REF(HIGH,0),10,0);
    ''''
    STICKLINE(C=O,H,L,0,0);
    STICKLINE((C=O)AND(C>REF(C,0)),C,O,8,0);
    STICKLINE((C=O)AND(C<REF(C,0)),C,O,8,0);
    STICKLINE(CROSS(ABC7,ABC14) AND ABC,CLOSE,OPEN,2,0),COLORMAGENTA;
    DRAWICON(CROSS(ABC7,ABC14) AND ABC,L*1.002,9);
    '''
    data['买']=CROSS(ABC7,ABC14)
    #DRAWTEXT(CROSS(ABC7,ABC14) AND{微信公众号:尊重市场}ABC,L*0.98,' ★买'),COLORMAGENTA;
    '''
    STICKLINE(CROSS(ABC25,ABC7),CLOSE,OPEN,2,0),COLORBLUE;
    CC:=ABS((2*CLOSE+HIGH+LOW)/4-MA(CLOSE,20))/MA(CLOSE,20);
    DD:=DMA(CLOSE,CC);
    上轨:(1+7/100)*DD,DOTLINE,COLORGREEN;
    下轨:(1-7/100)*DD,DOTLINE,COLORGREEN;
    中轨:(上轨+下轨)/2,DOTLINE,COLORGREEN;
    FK:(1+14/100)*DD,DOTLINE,COLORGRAY;
    CD:(1-14/100)*DD,DOTLINE,COLORGRAY;
    DRAWNUMBER(ISLASTBAR,上轨,上轨),COLOR00FFFF;
    DRAWNUMBER(ISLASTBAR,下轨,下轨),COLORFFFF00;
    DRAWNUMBER(ISLASTBAR,中轨,中轨),COLOR00FF00;
    DRAWNUMBER(ISLASTBAR,FK,FK){微信公众号:尊重市场},COLOR0000FF;
    DRAWNUMBER(ISLASTBAR,CD,CD),COLORWHITE;
    上轨绿:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    上轨红:IF(上轨>=REF(上轨,1),上轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
    中轨绿:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    中轨红:IF(中轨>=REF(中轨,1), 中轨,DRAWNULL),DOTLINE COLORRED, LINETHICK1;
    下轨绿:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE,COLORGREEN,LINETHICK1;
    下轨红:IF(下轨>=REF(下轨,1), 下轨,DRAWNULL),DOTLINE COLORRED,LINETHICK1;
    '''
    data['红色']=IF(ABC7>REF(ABC7,1),ABC7,None)
    data['绿色']=IF(ABC7<REF(ABC7,1),ABC7,None)
    data['buy']= data['买']
    data['hold']=data['红色']
    data['sell']=data['绿色']
    data['hold']= data['hold'].apply(lambda x : True if x!=None else False)
    data['sell']= data['sell'].apply(lambda x : True if x!=None else False)
    return data
```
测试的源代码

```
import akshare as ak
from tdx_strategy import tdx_strategy
df=ak.stock_zh_a_daily(symbol='sh600059',start_date='20240101')
print(tdx_strategy.take_advantage_of_the_trend(df))
```
测试的结果

```
           date      强      买        红色        绿色    buy   hold   sell
0    2024-01-02  False  False      None      None  False  False  False      
1    2024-01-03  False  False      None    9.2475  False  False   True      
2    2024-01-04  False  False      None  9.223125  False  False   True      
3    2024-01-05  False  False      None  9.179844  False  False   True      
4    2024-01-08  False  False      None  9.097383  False  False   True      
..          ...    ...    ...       ...       ...    ...    ...    ...      
141  2024-08-02  False  False  8.038147      None  False   True  False      
142  2024-08-05  False  False   8.04861      None  False   True  False      
143  2024-08-06  False  False  8.076458      None  False   True  False      
144  2024-08-07  False  False  8.119843      None  False   True  False      
145  2024-08-08  False  False  8.169883      None  False   True  False      

[146 rows x 8 columns]
```
![输入图片说明](image.png)
### 2​通达信【主力进场擒黑马】套装指标

【主力进场擒黑马】套装指标简介：
一、主图：

1、灰色K线，底部保持关注；

2、黄色K线，底部酌情买入；

3、红色K线，强势持仓阶段；

4、粉红K线，阶段开始减仓；

5、青色K线，准备清仓卖出；

6、绿色K线，要大跌，卖出；

7、“红色圆球”黑马启动信号；

8、白虚线上持有，线下休息。

二、副图：

1、红柱子，主力吸筹；

2、紫色柱，主力入场；

3、绿色柱，主力离场；

4、先吸筹，后入场买；

5、保留了“买、卖、追涨”等信号参考；

6、结合主图K线所表示意思参考使用。
![输入图片说明](image2.1.png)
![输入图片说明](image2.2.png)
![输入图片说明](image2.3.png)
1、主力进场擒黑马主图：
通达信源代码

```
MA5:MA(CLOSE,5)DOTLINE COLORWHITE;

N:=30;M:=13;

LC := REF(CLOSE,1);

RSI1:=SMA(MAX(CLOSE-LC,0),13,1)/SMA(ABS(CLOSE-LC),13,1)*100;

RSIF:=90-RSI1,COLOR33DD33;

A4:=((C-LLV(L,33))/(HHV(H,33)-LLV(L,33)))*67;

AAC22:=LLV(LOW,10);

AAC33:=HHV(HIGH,25);

动力线:=EMA((CLOSE-AAC22)/(AAC33-AAC22)*4,4);

RSV:=(C-LLV(L,9))/(HHV(H,9)-LLV(L,9))*100;

ABB1:=SMA(RSV,3,1){微信公众号:尊重市场};

ABB2:=SMA(ABB1,3,1);

ABB3:=3*ABB1-2*ABB2;

ABC1:=(LOW+HIGH+CLOSE*2)/4;

ABC2:= MA(ABC1,4);

ABC3:=HHV(ABC2,10);

ABC4:=MA(ABC3,3);

ABC5:=1.25*ABC4-0.25*ABC3;

XKKJ:=IF(ABC5>ABC3,ABC3,ABC5);

ACB1:=LLV(ABC2,10);

ACB2:=MA(ACB1,3);

ACB3:=1.25*ACB2-0.25*ACB1;

DKKJ:=IF(ACB3<ACB1,ACB1,ACB3);

MA13:=MA(C,13);{微信公众号:尊重市场}

ZDHM:=CROSS(C,DKKJ) AND CROSS(C,MA13) AND CROSS(C,XKKJ);

ZHM:=CROSS(C,MA13) AND CROSS(C,XKKJ);

DRAWICON((ZDHM OR ZHM),L,13);

STICKLINE((ZDHM OR ZHM),C,O,2,0),COLORMAGENTA;

DRAWTEXT((ZDHM OR ZHM),L,' ★黑马'),COLORYELLOW;

{微信公众号:尊重市场}

STICKLINE(动力线>0 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORGRAY;

STICKLINE(动力线>=0.2 AND 动力线<0.5 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORGRAY;

STICKLINE(动力线>=0.5 AND 动力线<1.75 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORYELLOW;

STICKLINE(动力线>=1.75 AND 动力线<3.2 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORRED;

STICKLINE(动力线>=3.2 AND 动力线<3.45 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORLIRED;

STICKLINE(动力线>=3.45,C,O,1,0) AND ABB3<ABB1,COLORCYAN;

STICKLINE(ABB3<ABB1,C,O,1,0),COLORGREEN;

```


2、主力进场擒黑马副图：
通达信源代码

```

N:=30;M:=13;

LC:=REF(CLOSE,1);

RSI1:=SMA(MAX(CLOSE-LC,0),13,1)/SMA(ABS(CLOSE-LC),13,1)*100;

RSIF:=90-RSI1,COLOR33DD33;

A4:=((C-LLV(L,33))/(HHV(H,33)-LLV(L,33)))*67;

ABC1:=(HHV(HIGH,9)-CLOSE)/(HHV(HIGH,9)-LLV(LOW,9))*100-70;

ABC2:=SMA(ABC1,9,1)+100;

ABC3:=(CLOSE-LLV(LOW,9))/(HHV(HIGH,9)-LLV(LOW,9))*100;

ABC4:=SMA(ABC3,3,1);

ABC5:=SMA(ABC4,3,1)+100;

ABC6:=ABC5-ABC2;{微信公众号:尊重市场}

趋势:IF(ABC6>45,ABC6-45,0);

STICKLINE(REF(趋势,1)< 趋势, 趋势,REF(趋势,1),2,0),COLORMAGENTA;

STICKLINE(REF(趋势,1)> 趋势, 趋势,REF(趋势,1),2,0),COLORGREEN;

强弱分界:=50,COLORFFFFCC;

底部:=0,COLOR00FFFF;

安全:=20,COLORFFFF66,LINETHICK1;

预警:=80,COLORFFFF66,LINETHICK1;

顶部:=100,COLORFFFF33;

V1:=LLV(LOW,10);

V2:=HHV(H,25);

价位线:=EMA((C-V1)/(V2-V1)*4,4);

DRAWTEXT(CROSS(价位线,0.3),20+4,'●买'),COLORRED;

DRAWTEXT(CROSS(3.5,价位线),趋势,'●卖'),COLORWHITE;

ABC2Q:=REF(LOW,1);

ABC3Q:=SMA(ABS(LOW-ABC2Q),3,1)/SMA(MAX(LOW-ABC2Q,0),3,1)*100;

ABC4Q:=EMA(IF(CLOSE*1.3,ABC3Q*10,ABC3Q/10),3);

ABC5Q:=LLV(LOW,30);

ABC6Q:=HHV(ABC4Q,30);

ABC7Q:=IF(MA(CLOSE,58),1,0);

ABC8Q:=EMA(IF(LOW<=ABC5Q,(ABC4Q+ABC6Q*2)/2,0),3)/618*ABC7Q;

ABC9Q:=IF(ABC8Q>100,100,ABC8Q);

ACB3:=(HHV(HIGH,21)-CLOSE)/(HHV(HIGH,21)-LLV(LOW,21))*100-10;

ACB4:=(CLOSE-LLV(LOW,21))/(HHV(HIGH,21)-LLV(LOW,21))*100;

ACB5:=SMA(ACB4,13,8);

走势:=CEILING(SMA(ACB5,13,8));

ACB6:=SMA(ACB3,21,8);{微信公众号:尊重市场}

卖临界:=STICKLINE(走势-ACB6>85,103,100,15,1),COLORRED,LINETHICK2;

主力线:=3*SMA((CLOSE-LLV(LOW,27))/(HHV(HIGH,27)-LLV(LOW,27))*100,5,1)-2*

SMA(SMA((CLOSE-LLV(LOW,27))/(HHV(HIGH,27)-LLV(LOW,27))*100,5,1),3,1),LINETHICK2,COLORBLUE;

超短线:=(((主力线-LLV(主力线,21))/(HHV(主力线,21)-LLV(主力线,21)))*(4))*(25),LINETHICK2,COLORBLUE;

ABC11:=REF((LOW+OPEN+CLOSE+HIGH)/4,1);

ABC21:=SMA(ABS(LOW-ABC11),13,1)/SMA(MAX(LOW-ABC11,0),10,1);

ABC31:=EMA(ABC21,10);

ABC41:=LLV(LOW,33);

ABC51:=EMA(IF(LOW<=ABC41,ABC31,0),3);

主力吸筹:IF(ABC51>REF(ABC51,1),ABC51,0),COLORRED,NODRAW;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,3,0 ),COLOR000055;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,2.6,0 ),COLOR000077;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,2.1,0 ),COLOR000099;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,1.5,0 ),COLOR0000BB;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,0.9,0 ),COLOR0000DD;

STICKLINE(ABC51>REF(ABC51,1),0,ABC51,0.3,0 ),COLOR0000FF;

ABC12:=3;{微信公众号:尊重市场}

ABC28:=(3)*(SMA(((CLOSE - LLV(LOW,27))/(HHV(HIGH,27) - LLV(LOW,27)))*(100),5,1)) - (2)*(SMA(SMA(((CLOSE - LLV(LOW,27))/(HHV(HIGH,27) - LLV(LOW,27)))*(100),5,1),3,1));

动态底部:=EMA(IF(L<= LLV(L,30),SMA(ABS(L-REF(L,1)),30,1)/SMA(MAX(L-REF(L,1),0),99,1),0)*5,3);

准备买入:=CROSS(C,(CLOSE,N,1)*1.02);

低点:IF(动态底部 AND 准备买入,50,0),COLORWHITE,LINETHICK3;

RSV11:=(CLOSE-LLV(LOW,19))/(HHV(HIGH,19)-LLV(LOW,19))*100;

K:=SMA(RSV11,3,1);

D:=SMA(K,3,1);

J:=3*K-2*D;

短线:=EMA(J,6),COLORRED;

浮筹:=MA(短线,28)*1,LINETHICK2,COLORGREEN;

空方:=MA(100*(HHV(HIGH,35)-CLOSE)/(HHV(HIGH,35)-LLV(LOW,35)),3),COLORYELLOW;

DRAWICON(CROSS(短线,浮筹) AND 短线<36,20+4,9),COLORBLUE, LINETHICK1;

DRAWTEXT(CROSS(浮筹,空方),浮筹,' 追'),COLORWHITE;

```


3、黑马启动信号选股：

```
QXS:=NOT(CODELIKE('4'));

QKC:=NOT(CODELIKE('688'));

QST:=IF(NAMELIKE('S'),0,1);

QXX:=IF(NAMELIKE('*'),0,1);

QBJ:=NOT(CODELIKE('8'));

AABB:=QXS AND QST AND QKC AND QXX AND QBJ;

N:=30;M:=13;

LC := REF(CLOSE,1);

RSI1:=SMA(MAX(CLOSE-LC,0),13,1)/SMA(ABS(CLOSE-LC),13,1)*100;

RSIF:=90-RSI1,COLOR33DD33;

A4:=((C-LLV(L,33))/(HHV(H,33)-LLV(L,33)))*67;

AAC22:=LLV(LOW,10);

AAC33:=HHV(HIGH,25);

动力线:=EMA((CLOSE-AAC22)/(AAC33-AAC22)*4,4);

RSV:=(C-LLV(L,9))/(HHV(H,9)-LLV(L,9))*100;

ABB1:=SMA(RSV,3,1){微信公众号:尊重市场};

ABB2:=SMA(ABB1,3,1);

ABB3:=3*ABB1-2*ABB2;

ABC1:=(LOW+HIGH+CLOSE*2)/4;

ABC2:= MA(ABC1,4);

ABC3:=HHV(ABC2,10);

ABC4:=MA(ABC3,3);

ABC5:=1.25*ABC4-0.25*ABC3;

XKKJ:=IF(ABC5>ABC3,ABC3,ABC5);

ACB1:=LLV(ABC2,10);

ACB2:=MA(ACB1,3);

ACB3:=1.25*ACB2-0.25*ACB1;

DKKJ:=IF(ACB3<ACB1,ACB1,ACB3);

MA13:=MA(C,13);{微信公众号:尊重市场}

尊重市场M:=CROSS(C,DKKJ) AND CROSS(C,MA13) AND CROSS(C,XKKJ);

ZHM:=CROSS(C,MA13) AND CROSS(C,XKKJ);

黑马:(尊重市场M OR ZHM) AND AABB;


```
python 源代码解析
1、主力进场擒黑马主图：

```
from xg_tdx_func.xg_tdx_func import *
from trader_tool.unification_data import unification_data
class main_approach_to_capture_dark_horse_main_figure:
    def __init__(self,df):
        self.df=df
    def main_approach_to_capture_dark_horse_main_figure(self):
        '''
        一、主图：
        1、灰色K线，底部保持关注；
        2、黄色K线，底部酌情买入；
        3、红色K线，强势持仓阶段；
        4、粉红K线，阶段开始减仓；
        5、青色K线，准备清仓卖出；
        6、绿色K线，要大跌，卖出；
        7、“红色圆球”黑马启动信号；
        8、白虚线上持有，线下休息。
        二、副图：
        1、红柱子，主力吸筹；
        2、紫色柱，主力入场；
        3、绿色柱，主力离场；
        4、先吸筹，后入场买；
        5、保留了“买、卖、追涨”等信号参考；
        输出MA5:收盘价的5日简单移动平均DOTLINE 画白色
        N赋值:30
        M赋值:13
        赋值: 1日前的收盘价
        RSI1赋值:收盘价-LC和0的较大值的13日[1日权重]移动平均/收盘价-LC的绝对值的13日[1日权重]移动平均*100
        RSIF赋值:90-RSI1,COLOR33DD33
        A4赋值:((收盘价-33日内最低价的最低值)/(33日内最高价的最高值-33日内最低价的最低值))*67
        AAC22赋值:10日内最低价的最低值
        AAC33赋值:25日内最高价的最高值
        动力线赋值:(收盘价-AAC22)/(AAC33-AAC22)*4的4日指数移动平均
        RSV赋值:(收盘价-9日内最低价的最低值)/(9日内最高价的最高值-9日内最低价的最低值)*100
        ABB1赋值:RSV的3日[1日权重]移动平均
        ABB2赋值:ABB1的3日[1日权重]移动平均
        ABB3赋值:3*ABB1-2*ABB2
        ABC1赋值:(最低价+最高价+收盘价*2)/4
        ABC2赋值: ABC1的4日简单移动平均
        ABC3赋值:10日内ABC2的最高值
        ABC4赋值:ABC3的3日简单移动平均
        ABC5赋值:1.25*ABC4-0.25*ABC3
        XKKJ赋值:如果ABC5>ABC3,返回ABC3,否则返回ABC5
        ACB1赋值:10日内ABC2的最低值
        ACB2赋值:ACB1的3日简单移动平均
        ACB3赋值:1.25*ACB2-0.25*ACB1
        DKKJ赋值:如果ACB3<ACB1,返回ACB1,否则返回ACB3
        MA13赋值:收盘价的13日简单移动平均
        ZDHM赋值:收盘价上穿DKKJ AND 收盘价上穿MA13 AND 收盘价上穿XKKJ
        ZHM赋值:收盘价上穿MA13 AND 收盘价上穿XKKJ
        当满足条件(ZDHMORZHM)时,在最低价位置画13号图标
        当满足条件(ZDHMORZHM)时,在收盘价和开盘价位置之间画柱状线,宽度为2,0不为0则画空心柱.,画洋红色
        当满足条件(ZDHMORZHM)时,在最低价位置书写文字,画黄色

        当满足条件动力线>0AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画深灰色
        当满足条件动力线>=0.2AND动力线<0.5AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画深灰色
        当满足条件动力线>=0.5AND动力线<1.75AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画黄色
        当满足条件动力线>=1.75AND动力线<3.2AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画红色
        当满足条件动力线>=3.2AND动力线<3.45AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画淡红色
        当满足条件动力线>=3.45时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱. AND ABB3<ABB1,画青色
        当满足条件ABB3<ABB1时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画绿色
        '''
        df=self.df
        CLOSE=df['close']
        C=df['close']
        LOW=df['low']
        L=df['low']
        HIGH=df['high']
        H=df['high']
        OPEN=df['open']
        O=df['open']
        volume=df['volume']
        V=df['volume']
        MA5=MA(CLOSE,5)#DOTLINE COLORWHITE;
        N=30
        M=13
        LC = REF(CLOSE,1)
        RSI1=SMA(MAX(CLOSE-LC,0),13,1)/SMA(ABS(CLOSE-LC),13,1)*100
        RSIF=90-RSI1#COLOR33DD33;
        A4=((C-LLV(L,33))/(HHV(H,33)-LLV(L,33)))*67
        AAC22=LLV(LOW,10)
        AAC33=HHV(HIGH,25)
        动力线=EMA((CLOSE-AAC22)/(AAC33-AAC22)*4,4)
        RSV=(C-LLV(L,9))/(HHV(H,9)-LLV(L,9))*100
        ABB1=SMA(RSV,3,1)
        ABB2=SMA(ABB1,3,1)
        ABB3=3*ABB1-2*ABB2
        ABC1=(LOW+HIGH+CLOSE*2)/4
        ABC2= MA(ABC1,4)
        ABC3=HHV(ABC2,10)
        ABC4=MA(ABC3,3)
        ABC5=1.25*ABC4-0.25*ABC3
        XKKJ=IF(ABC5>ABC3,ABC3,ABC5)
        ACB1=LLV(ABC2,10)
        ACB2=MA(ACB1,3)
        ACB3=1.25*ACB2-0.25*ACB1
        DKKJ=IF(ACB3<ACB1,ACB1,ACB3)
        MA13=MA(C,13)
        ZDHM=AND(AND(CROSS(C,DKKJ),CROSS(C,MA13)) ,CROSS(C,XKKJ))
        ZHM=AND(CROSS(C,MA13),CROSS(C,XKKJ))
        #DRAWICON((ZDHM OR ZHM),L,13);
        #STICKLINE((ZDHM OR ZHM),C,O,2,0),COLORMAGENTA;
        #DRAWTEXT((ZDHM OR ZHM),L,' ★黑马'),COLORYELLOW;
        df['黑马']=IF(OR(ZDHM,ZHM),True,False)[1:]
        #当满足条件动力线>0AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画深灰色
        #STICKLINE(动力线>0 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORGRAY;
        df['深灰色']=OR(AND(动力线>0,AND(ABB3>ABB1,ABB3<REF(ABB3,1))),ABB3>ABB1)
        #当满足条件动力线>=0.2AND动力线<0.5AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画深灰色
        #STICKLINE(动力线>=0.2 AND 动力线<0.5 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORGRAY;
        df['深灰色']=OR(AND(AND(动力线>=0.2,动力线<0.5),AND(ABB3>ABB1 , ABB3<REF(ABB3,1))),ABB3>ABB1)
        #当满足条件动力线>=0.5AND动力线<1.75AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画黄色
        #STICKLINE(动力线>=0.5 AND 动力线<1.75 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORYELLOW;
        df['黄色']=OR(AND(AND(动力线>=0.5,动力线<1.75),AND(ABB3>ABB1,ABB3<REF(ABB3,1))),ABB3>ABB1)
        #当满足条件动力线>=1.75AND动力线<3.2AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画红色
        #STICKLINE(动力线>=1.75 AND 动力线<3.2 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORRED;
        df['红色']=OR(AND(AND(动力线>=1.75 ,动力线<3.2) ,AND(ABB3>ABB1,ABB3<REF(ABB3,1))), ABB3>ABB1)
        #当满足条件动力线>=3.2AND动力线<3.45AND((ABB3>ABB1ANDABB3<1日前的ABB3)ORABB3>ABB1)时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画淡红色
        #STICKLINE(动力线>=3.2 AND 动力线<3.45 AND ((ABB3>ABB1 AND ABB3<REF(ABB3,1)) OR ABB3>ABB1),C,O,1,0),COLORLIRED;
        df['淡红色']=OR(AND(AND(动力线>=3.2,动力线<3.45),AND(ABB3>ABB1,ABB3<REF(ABB3,1))) , ABB3>ABB1)
        #当满足条件动力线>=3.45时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱. AND ABB3<ABB1,画青色
        #STICKLINE(动力线>=3.45,C,O,1,0) AND ABB3<ABB1,COLORCYAN;
        df['青色']=AND(动力线>=3.45,ABB3<ABB1)
        #当满足条件ABB3<ABB1时,在收盘价和开盘价位置之间画柱状线,宽度为1,0不为0则画空心柱.,画绿色
        #STICKLINE(ABB3<ABB1,C,O,1,0),COLORGREEN;
        df['绿色']=ABB3<ABB1
        return df 
if __name__=='__main__':
    data=unification_data(trader_tool='ths')
    data=data.get_unification_data()
    df=data.get_hist_data_em(stock='513100')
    modes=main_approach_to_capture_dark_horse_main_figure(df=df)
    result=modes.main_approach_to_capture_dark_horse_main_figure()
    print(result)

```
解析的结果

```
           date   open  close   high    low   volume          成交额    振幅   涨跌幅    涨跌额   换手率     黑马    深灰色     黄色     红色    淡红色  
   青色     绿色
0     2013-05-15  0.198  0.199  0.200  0.198   877116   87379278.0  1.00 -0.50 -0.001  0.93  False  False  False  False  False  False  False
1     2013-05-16  0.199  0.200  0.200  0.199   265570   26501420.0  0.50  0.50  0.001  0.28  False  False  False  False  False  False  False
2     2013-05-17  0.200  0.199  0.200  0.199    36379    3630062.0  0.50 -0.50 -0.001  0.04  False  False  False  False  False  False  False
3     2013-05-20  0.200  0.199  0.201  0.199    80490    8033039.0  1.01  0.00  0.000  0.09  False  False  False  False  False  False  False
4     2013-05-21  0.199  0.199  0.200  0.199   161124   16023195.0  0.50  0.00  0.000  0.17  False  False  False  False  False  False  False
...          ...    ...    ...    ...    ...      ...          ...   ...   ...    ...   ...    ...    ...    ...    ...    ...    ...    ...
2755  2024-09-09  1.349  1.364  1.366  1.343  5481686  741657926.0  1.68 -0.58 -0.008  5.80  False  False  False  False  False  False   True
2756  2024-09-10  1.375  1.368  1.377  1.367  4460372  612238602.0  0.73  0.29  0.004  4.72  False  False  False  False  False  False   True
2757  2024-09-11  1.381  1.377  1.382  1.369  4461679  613644268.0  0.95  0.66  0.009  4.72  False   True   True   True   True  False  False
2758  2024-09-12  1.414  1.421  1.423  1.412  6535594  925906001.0  0.80  3.20  0.044  6.92  False   True   True   True   True  False  False
2759  2024-09-13  1.432  1.435  1.437  1.429  4521259  647791882.0  0.56  0.99  0.014  4.79  False   True   True   True   True  False  False

[2760 rows x 18 columns]
```
### 3【六脉神剑3.0】指标简介
1、六脉神剑指的是MACD+KDJ+RSI+LWR+BBI+ZLMM，这6个指标同时共振出现的买点信号。

2、六脉神剑3.0版本，也算是在上一版【六脉神剑】指标基础上优化而来，并增强了捕捉信号的敏感度，提高了成功率。

3、六指标同时共振买入时，显示“红箭头”同时向上，并带有“黄柱”买入信号；洋红色覆盖表示可“持有”的天数。



指标无未来函数，以下举例：
![输入图片说明](image3.1.png)
![输入图片说明](image3.2.png)
![输入图片说明](image3.3.png)
通达信源代码

```
DIFF:=EMA(CLOSE,8)-EMA(CLOSE,13);

DEA:=EMA(DIFF,5);

DRAWICON(DIFF>DEA,1,1);

DRAWICON(DIFF<DEA,1,2);

DRAWTEXT(ISLASTBAR=1,1,'. MACD'),COLORFFFFFF;{微信公众号:尊重市场}

ABC1:=DIFF>DEA;

尊重市场1:=(CLOSE-LLV(LOW,8))/(HHV(HIGH,8)-LLV(LOW,8))*100;

K:=SMA(尊重市场1,3,1);

D:=SMA(K,3,1);

DRAWICON(K>D,2,1);

DRAWICON(K<D,2,2);

DRAWTEXT(ISLASTBAR=1,2,'. KDJ'),COLORFFFFFF;

ABC2:=K>D;

指标营地:=REF(CLOSE,1);

RSI1:=(SMA(MAX(CLOSE-指标营地,0),5,1))/(SMA(ABS(CLOSE-指标营地),5,1))*100;

RSI2:=(SMA(MAX(CLOSE-指标营地,0),13,1))/(SMA(ABS(CLOSE-指标营地),13,1))*100;

DRAWICON(RSI1>RSI2,3,1);

DRAWICON(RSI1<RSI2,3,2);

DRAWTEXT(ISLASTBAR=1,3,'. RSI'),COLORFFFFFF;

ABC3:=RSI1>RSI2;

尊重市场:=-(HHV(HIGH,13)-CLOSE)/(HHV(HIGH,13)-LLV(LOW,13))*100;

LWR1:=SMA(尊重市场,3,1);

LWR2:=SMA(LWR1,3,1);

DRAWICON(LWR1>LWR2,4,1);

DRAWICON(LWR1<LWR2,4,2);

DRAWTEXT(ISLASTBAR=1,4,'. LWR'),COLORFFFFFF;

ABC4:=LWR1>LWR2;

BBI:=(MA(CLOSE,3)+MA(CLOSE,5)+MA(CLOSE,8)+MA(CLOSE,13))/4;

DRAWICON(CLOSE>BBI,5,1);

DRAWICON(CLOSE<BBI,5,2);

DRAWTEXT(ISLASTBAR=1,5,'. BBI'),COLORFFFFFF;

ABC10:=7;

ABC5:=CLOSE>BBI;

MTM:=CLOSE-REF(CLOSE,1);

MMS:=100*EMA(EMA(MTM,5),3)/EMA(EMA(ABS(MTM),5),3);

MMM:=100*EMA(EMA(MTM,13),8)/EMA(EMA(ABS(MTM),13),8);

DRAWICON(MMS>MMM,6,1);

DRAWICON(MMS<MMM,6,2);

DRAWTEXT(ISLASTBAR=1,6,'. ZLMM'),COLORFFFFFF;

ABC6:=MMS>MMM;{微信公众号:尊重市场}

买入:IF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6=1  

AND REF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6,1)=0,6,0),COLORYELLOW,LINETHICK2;

持有:IF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6,6,0),COLORMAGENTA,LINETHICK2;

共振:=ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6 ;

STICKLINE(共振,0,6,0.6,1),COLORMAGENTA;

STICKLINE(买入,0,6,0.6,0),COLORYELLOW;

DRAWICON(DIFF>DEA,1,1);

DRAWICON(DIFF<DEA,1,2);

DRAWICON(K>D,2,1);

DRAWICON(K<D,2,2);

DRAWICON(RSI1>RSI2,3,1);

DRAWICON(RSI1<RSI2,3,2);

DRAWICON(LWR1>LWR2,4,1);

DRAWICON(LWR1<LWR2,4,2);

DRAWICON(CLOSE>BBI,5,1);

DRAWICON(CLOSE<BBI,5,2);

DRAWICON(MMS>MMM,6,1);{微信公众号:尊重市场}

DRAWICON(MMS<MMM,6,2);

DRAWICON(买入,6.6,9);
```
2、六脉神剑3.0选股指标：

```
DIFF:=EMA(CLOSE,8)-EMA(CLOSE,13);

DEA:=EMA(DIFF,5);{微信公众号:尊重市场}

ABC1:=DIFF>DEA;

尊重市场1:=(CLOSE-LLV(LOW,8))/(HHV(HIGH,8)-LLV(LOW,8))*100;

K:=SMA(尊重市场1,3,1);

D:=SMA(K,3,1);

ABC2:=K>D;

指标营地:=REF(CLOSE,1);

RSI1:=(SMA(MAX(CLOSE-指标营地,0),5,1))/(SMA(ABS(CLOSE-指标营地),5,1))*100;

RSI2:=(SMA(MAX(CLOSE-指标营地,0),13,1))/(SMA(ABS(CLOSE-指标营地),13,1))*100;

ABC3:=RSI1>RSI2;

尊重市场:=-(HHV(HIGH,13)-CLOSE)/(HHV(HIGH,13)-LLV(LOW,13))*100;

LWR1:=SMA(尊重市场,3,1);

LWR2:=SMA(LWR1,3,1);

ABC4:=LWR1>LWR2;

BBI:=(MA(CLOSE,3)+MA(CLOSE,5)+MA(CLOSE,8)+MA(CLOSE,13))/4;

ABC10:=7;

去新三板:=NOT(CODELIKE('4'));

去科创板:=NOT(CODELIKE('688'));

去ST:=IF(NAMELIKE('S'),0,1);

去星星:=IF(NAMELIKE('*'),0,1);

去北交:=NOT(CODELIKE('8'));

条件限定:=去新三板 AND 去ST AND 去科创板 AND 去星星 AND 去北交;

ABC5:=CLOSE>BBI;

MTM:=CLOSE-REF(CLOSE,1);

MMS:=100*EMA(EMA(MTM,5),3)/EMA(EMA(ABS(MTM),5),3);

MMM:=100*EMA(EMA(MTM,13),8)/EMA(EMA(ABS(MTM),13),8);

ABC6:=MMS>MMM;{微信公众号:尊重市场}

六脉共振:IF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6=1

AND REF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6,1)=0,6,0) AND 条件限定;


```
python源代码解析

```
from xg_tdx_func.xg_tdx_func import *
from trader_tool.unification_data import unification_data
class six_pulse_excalibur_hist:
    def __init__(self,df):
        '''
        六脉神剑
        '''
        self.df=df
    def six_pulse_excalibur_hist(self):
        '''
        六脉神剑
        DIFF赋值:收盘价的8日指数移动平均-收盘价的13日指数移动平均
        DEA赋值:DIFF的5日指数移动平均
        当满足条件DIFF>DEA时,在1位置画1号图标
        当满足条件DIFF<DEA时,在1位置画2号图标
        当满足条件是否最后一个周期=1时,在1位置书写文字,COLORFFFFFF
        ABC1赋值:DIFF>DEA
        尊重市场1赋值:(收盘价-8日内最低价的最低值)/(8日内最高价的最高值-8日内最低价的最低值)*100
        K赋值:尊重市场1的3日[1日权重]移动平均
        D赋值:K的3日[1日权重]移动平均
        当满足条件K>D时,在2位置画1号图标
        当满足条件K<D时,在2位置画2号图标
        当满足条件是否最后一个周期=1时,在2位置书写文字,COLORFFFFFF
        ABC2赋值:K>D
        指标营地赋值:1日前的收盘价
        RSI1赋值:(收盘价-指标营地和0的较大值的5日[1日权重]移动平均)/(收盘价-指标营地的绝对值的5日[1日权重]移动平均)*100
        RSI2赋值:(收盘价-指标营地和0的较大值的13日[1日权重]移动平均)/(收盘价-指标营地的绝对值的13日[1日权重]移动平均)*100
        当满足条件RSI1>RSI2时,在3位置画1号图标
        当满足条件RSI1<RSI2时,在3位置画2号图标
        当满足条件是否最后一个周期=1时,在3位置书写文字,COLORFFFFFF
        ABC3赋值:RSI1>RSI2
        尊重市场赋值:-(13日内最高价的最高值-收盘价)/(13日内最高价的最高值-13日内最低价的最低值)*100
        LWR1赋值:尊重市场的3日[1日权重]移动平均
        LWR2赋值:LWR1的3日[1日权重]移动平均
        当满足条件LWR1>LWR2时,在4位置画1号图标
        当满足条件LWR1<LWR2时,在4位置画2号图标
        当满足条件是否最后一个周期=1时,在4位置书写文字,COLORFFFFFF
        ABC4赋值:LWR1>LWR2
        BBI赋值:(收盘价的3日简单移动平均+收盘价的5日简单移动平均+收盘价的8日简单移动平均+收盘价的13日简单移动平均)/4
        当满足条件收盘价>BBI时,在5位置画1号图标
        当满足条件收盘价<BBI时,在5位置画2号图标
        当满足条件是否最后一个周期=1时,在5位置书写文字,COLORFFFFFF
        ABC10赋值:7
        ABC5赋值:收盘价>BBI
        MTM赋值:收盘价-1日前的收盘价
        MMS赋值:100*MTM的5日指数移动平均的3日指数移动平均/MTM的绝对值的5日指数移动平均的3日指数移动平均
        MMM赋值:100*MTM的13日指数移动平均的8日指数移动平均/MTM的绝对值的13日指数移动平均的8日指数移动平均
        当满足条件MMS>MMM时,在6位置画1号图标
        当满足条件MMS<MMM时,在6位置画2号图标
        当满足条件是否最后一个周期=1时,在6位置书写文字,COLORFFFFFF
        ABC6赋值:MMS>MMM
        输出买入:如果ABC1ANDABC2ANDABC3ANDABC4ANDABC5ANDABC6=1ANDREF(ABC1ANDABC2ANDABC3ANDABC4ANDABC5ANDABC6,1)=0,返回6,否则返回0,画黄色,线宽为2
        输出持有:如果ABC1ANDABC2ANDABC3ANDABC4ANDABC5ANDABC6,返回6,否则返回0,画洋红色,线宽为2
        共振赋值:ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6 
        当满足条件共振时,在0和6位置之间画柱状线,宽度为0.6,1不为0则画空心柱.,画洋红色
        当满足条件买入时,在0和6位置之间画柱状线,宽度为0.6,0不为0则画空心柱.,画黄色
        当满足条件DIFF>DEA时,在1位置画1号图标
        当满足条件DIFF<DEA时,在1位置画2号图标
        当满足条件K>D时,在2位置画1号图标
        当满足条件K<D时,在2位置画2号图标
        当满足条件RSI1>RSI2时,在3位置画1号图标
        当满足条件RSI1<RSI2时,在3位置画2号图标
        当满足条件LWR1>LWR2时,在4位置画1号图标
        当满足条件LWR1<LWR2时,在4位置画2号图标
        当满足条件收盘价>BBI时,在5位置画1号图标
        当满足条件收盘价<BBI时,在5位置画2号图标
        当满足条件MMS>MMM时,在6位置画1号图标
        当满足条件MMS<MMM时,在6位置画2号图标
        当满足条件买入时,在6.6位置画9号图标
        '''
        df=self.df
        markers=0
        signal=0
        #df=self.data.get_hist_data_em(stock=stock)
        CLOSE=df['close']
        LOW=df['low']
        HIGH=df['high']
        DIFF=EMA(CLOSE,8)-EMA(CLOSE,13)
        DEA=EMA(DIFF,5)
        #如果满足DIFF>DEA 在1的位置标记1的图标
        #DRAWICON(DIFF>DEA,1,1);
        markers+=IF(DIFF>DEA,1,0)
        #如果满足DIFF<DEA 在1的位置标记2的图标
        #DRAWICON(DIFF<DEA,1,2);
        markers+=IF(DIFF<DEA,1,0)
        #DRAWTEXT(ISLASTBAR=1,1,'. MACD'),COLORFFFFFF;{微信公众号:尊重市场}
        ABC1=DIFF>DEA
        signal+=IF(ABC1,1,0)
        尊重市场1=(CLOSE-LLV(LOW,8))/(HHV(HIGH,8)-LLV(LOW,8))*100
        K=SMA(尊重市场1,3,1)
        D=SMA(K,3,1)
        #如果满足k>d 在2的位置标记1的图标
        markers+=IF(K>D,1,0)
        #DRAWICON(K>D,2,1);
        markers+=IF(K<D,1,0)
        #DRAWICON(K<D,2,2);
        #DRAWTEXT(ISLASTBAR=1,2,'. KDJ'),COLORFFFFFF;
        ABC2=K>D
        signal+=IF(ABC2,1,0)
        指标营地=REF(CLOSE,1)
        RSI1=(SMA(MAX(CLOSE-指标营地,0),5,1))/(SMA(ABS(CLOSE-指标营地),5,1))*100
        RSI2=(SMA(MAX(CLOSE-指标营地,0),13,1))/(SMA(ABS(CLOSE-指标营地),13,1))*100
        markers+=IF(RSI1>RSI2,1,0)
        #DRAWICON(RSI1>RSI2,3,1);
        markers+=IF(RSI1<RSI2,1,0)
        #DRAWICON(RSI1<RSI2,3,2);
        #DRAWTEXT(ISLASTBAR=1,3,'. RSI'),COLORFFFFFF;
        ABC3=RSI1>RSI2
        signal+=IF(ABC3,1,0)
        尊重市场=-(HHV(HIGH,13)-CLOSE)/(HHV(HIGH,13)-LLV(LOW,13))*100
        LWR1=SMA(尊重市场,3,1)
        LWR2=SMA(LWR1,3,1)
        #DRAWICON(LWR1>LWR2,4,1);
        markers+=IF(LWR1>LWR2,1,0)
        #DRAWICON(LWR1<LWR2,4,2);
        markers+=IF(LWR1<LWR2,1,0)
        #DRAWTEXT(ISLASTBAR=1,4,'. LWR'),COLORFFFFFF;
        ABC4=LWR1>LWR2
        signal+=IF(ABC4,1,0)
        BBI=(MA(CLOSE,3)+MA(CLOSE,5)+MA(CLOSE,8)+MA(CLOSE,13))/4
        #DRAWICON(CLOSE>BBI,5,1);
        markers+=IF(CLOSE>BBI,1,0)
        #DRAWICON(CLOSE<BBI,5,2);
        markers+=IF(CLOSE<BBI,1,0)
        #DRAWTEXT(ISLASTBAR=1,5,'. BBI'),COLORFFFFFF;
        ABC10=7
        ABC5=CLOSE>BBI
        signal+=IF(ABC5,1,0)
        MTM=CLOSE-REF(CLOSE,1)
        MMS=100*EMA(EMA(MTM,5),3)/EMA(EMA(ABS(MTM),5),3)
        MMM=100*EMA(EMA(MTM,13),8)/EMA(EMA(ABS(MTM),13),8)
        markers+=IF(MMS>MMM,1,0)
        #DRAWICON(MMS>MMM,6,1);
        markers+=IF(MMS<MMM,1,0)
        #DRAWICON(MMS<MMM,6,2);
        #DRAWTEXT(ISLASTBAR=1,6,'. ZLMM'),COLORFFFFFF;
        ABC6=MMS>MMM
        signal+=IF(ABC6,1,0)
        '''
        买入:IF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6=1  
        AND REF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6,1)=0,6,0),COLORYELLOW,LINETHICK2;
        持有:IF(ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6,6,0),COLORMAGENTA,LINETHICK2;
        共振:=ABC1 AND ABC2 AND ABC3 AND ABC4 AND ABC5 AND ABC6 ;
        STICKLINE(共振,0,6,0.6,1),COLORMAGENTA;
        STICKLINE(买入,0,6,0.6,0),COLORYELLOW;
        DRAWICON(DIFF>DEA,1,1);
        DRAWICON(DIFF<DEA,1,2);
        DRAWICON(K>D,2,1);
        DRAWICON(K<D,2,2);
        DRAWICON(RSI1>RSI2,3,1);
        DRAWICON(RSI1<RSI2,3,2);
        DRAWICON(LWR1>LWR2,4,1);
        DRAWICON(LWR1<LWR2,4,2);
        DRAWICON(CLOSE>BBI,5,1);
        DRAWICON(CLOSE<BBI,5,2);
        DRAWICON(MMS>MMM,6,1);{微信公众号:尊重市场}
        DRAWICON(MMS<MMM,6,2);
        DRAWICON(买入,6.6,9);
        '''
        df['signal']=signal
        df['markers']=markers
        return df
if __name__=='__main__':
    data=unification_data(trader_tool='ths')
    data=data.get_unification_data()
    df=data.get_hist_data_em(stock='513100')
    modes=six_pulse_excalibur_hist(df=df)
    result=modes.six_pulse_excalibur_hist()
    print(result)
    
```
解析结果

```
操作方式,登录qmt,选择行情加交易选,择极简模式
作者:小果
作者微信:15117320079,开实盘qmt可以联系我,开户也可以
作者微信公众号:数据分析与运用
公众号链接:https://mp.weixin.qq.com/s/rxGJpZYxdUIHitjvI-US1A 
作者知识星球:金融量化交易研究院  https://t.zsxq.com/19VzjjXNi
通达信数据连接成功
通达信数据连接成功
通达信数据连接成功
            date   open  close   high    low   volume          成交额    振幅   涨跌幅    涨跌额   换手率  signal  markers
0     2013-05-15  0.198  0.199  0.200  0.198   877116   87379278.0  1.00 -0.50 -0.001  0.93       0        0
1     2013-05-16  0.199  0.200  0.200  0.199   265570   26501420.0  0.50  0.50  0.001  0.28       1        1
2     2013-05-17  0.200  0.199  0.200  0.199    36379    3630062.0  0.50 -0.50 -0.001  0.04       1        3
3     2013-05-20  0.200  0.199  0.201  0.199    80490    8033039.0  1.01  0.00  0.000  0.09       0        3
4     2013-05-21  0.199  0.199  0.200  0.199   161124   16023195.0  0.50  0.00  0.000  0.17       0        3
...          ...    ...    ...    ...    ...      ...          ...   ...   ...    ...   ...     ...      ...
2755  2024-09-09  1.349  1.364  1.366  1.343  5481686  741657926.0  1.68 -0.58 -0.008  5.80       0        6
2756  2024-09-10  1.375  1.368  1.377  1.367  4460372  612238602.0  0.73  0.29  0.004  4.72       1        6
2757  2024-09-11  1.381  1.377  1.382  1.369  4461679  613644268.0  0.95  0.66  0.009  4.72       2        6
2758  2024-09-12  1.414  1.421  1.423  1.412  6535594  925906001.0  0.80  3.20  0.044  6.92       6        6
2759  2024-09-13  1.432  1.435  1.437  1.429  4521259  647791882.0  0.56  0.99  0.014  4.79       6        6

[2760 rows x 13 columns]
```
### 4通达信【十全十美】
【十全十美】指标设计原理：核心逻辑是共振。
1个形态指标+3个量化模型+6个传统指标。
(MACD,KDJ,RSI,LWR,BBI,MTM),这十个模型都显示市场趋势处于上行趋势的时候,也就是十维共振向上的时候,代表着机会来临，可以把握;
反之，则代表风险来临,应该规避。
这种逻辑也暗合投资之道【顺势而为】的交易真言。

什么是《十全十美》1个趋势、3个量化、6个指标。
一个趋势指标：
1、趋势转为上行趋势，经常会出现一些典型的形态。
2、包括放量突破、阳吞阴、黄金坑形态等等，这些经典的趋势转多的形态汇总。

三个量化模型 ：
1、多空博弈量化模型：多头占优优势。
2、龙腾四海量化模型：龙腾线50以上。
3、量能突破量化模型：成交量持续放量。

六个经典技术指标 ：
1、MACD、KDJ、RSI、LWR、BBI、MTM6个指标。
2、还有流通市值、主力控盘度等指标。

指标图形信号意义：
1、黄色代表三维量化模型共振.
2、紫色代表量化模型和技术指标的九维共振
3、钱袋子代表十维共振

指标使用口诀：
机会来临出现钱袋子信号，十维共振信号
谨慎减仓信号，紫色方块信号消失，十维共振结束
清仓信号，黄色方块信号消失，三维量化共振结束

指标操盘秘籍：
1、指数低位启动，或者上行趋势良好，积极参与；
2、如果指数处于高位，从高位往下走，呈现下跌趋势，下跌趋势的反抽行情，尽量回避，行情难持续。
3、符合当日主线行情的优先考虑。
4、同一个概念或者行业，选股出现两个以上标的的时候，这种属于共振效应，可以优先考虑。
5、中小市值，基本面优秀、业绩稳定的优先考虑。
6、当日盘中热点优先考虑，一般就是指板块涨停个股最多的板块！
![输入图片说明](image4.1.png)
![输入图片说明](image4.1.png)
![输入图片说明](image4.3.png)
![输入图片说明](image4.4.png)
通达信源代码
1、十全十美幅图指标：

```
{微信公众号:尊重市场}

DRAWGBK(C>1,RGB(0,0,0),RGB(1,1,1),1,0,0);

DIFF:=EMA(CLOSE,8)-EMA(CLOSE,13);

DEA:=EMA(DIFF,5);

A1:STICKLINE(DIFF>DEA,5,10,2,1),COLORRED;

STICKLINE(DIFF<DEA,5,10,2,1),COLORGREEN;

尊重市场1:=(CLOSE-LLV(LOW,8))/(HHV(HIGH,8)-LLV(LOW,8))*100;

K:=SMA(尊重市场1,3,1);

D:=SMA(K,3,1);

A2:STICKLINE(K>D,10,15,2,1),COLORRED;

STICKLINE(K<D,10,15,2,1),COLORGREEN;

LC:=REF(CLOSE,1);

CBA1:=(SMA(MAX(CLOSE-LC,0),5,1))/(SMA(ABS(CLOSE-LC),5,1))*100;

CBA2:=(SMA(MAX(CLOSE-LC,0),13,1))/(SMA(ABS(CLOSE-LC),13,1))*100;

A3:STICKLINE(CBA1>CBA2,15,20,2,1),COLORRED;

STICKLINE(CBA1<CBA2,15,20,2,1),COLORGREEN;

{微信公众号:尊重市场}

尊重市场:=-(HHV(HIGH,13)-CLOSE)/(HHV(HIGH,13)-LLV(LOW,13))*100;

指标营地1:=SMA(尊重市场,3,1);

指标营地2:=SMA(指标营地1,3,1);

A4:STICKLINE(指标营地1>指标营地2,20,25,2,1),COLORRED;

STICKLINE(指标营地1<指标营地2,20,25,2,1),COLORGREEN;

BBI:=(MA(CLOSE,3)+MA(CLOSE,6)+MA(CLOSE,12)+MA(CLOSE,24))/4;

A5:STICKLINE(CLOSE>BBI,25,30,2,1),COLORRED;

STICKLINE(CLOSE<BBI,25,30,2,1),COLORGREEN;

{微信公众号:尊重市场}

MTM:=CLOSE-REF(CLOSE,1);

MMS:=100*EMA(EMA(MTM,5),3)/EMA(EMA(ABS(MTM),5),3);

MMM:=100*EMA(EMA(MTM,13),8)/EMA(EMA(ABS(MTM),13),8);

A6:STICKLINE(MMS>MMM,30,35,2,1),COLORRED;

STICKLINE(MMS<MMM,30,35,2,1),COLORGREEN;

MAV:=(C*2+H+L)/4;

SK:=EMA(MAV,13)-EMA(MAV,34);

SD:=EMA(SK,5);

空方主力:=(-2*(SK-SD))*3.8,COLORGREEN;

多方主力:=(2*(SK-SD))*3.8,COLORRED;

A7:STICKLINE(多方主力>空方主力,37,42,2,0),COLORRED;

STICKLINE(多方主力<空方主力,37,42,2,0),COLORGREEN;

MA11:=MA(CLOSE,5);

MA22:=REF(MA(CLOSE,5),1);

STICKLINE(MA22>MA11,43,48,2,0),COLORGREEN;

A8:STICKLINE(MA22<= MA11,43,48,2,0),COLORRED;

XYZ2:=IF(MONTH<12,1,1);

XYZ3:=(2*CLOSE+HIGH+LOW)/4;

XYZ4:=LLV(LOW,34);

XYZ5:=HHV(HIGH,34);

主力:=EMA((XYZ3-XYZ4)/(XYZ5-XYZ4)*100,13)*XYZ2;

散户:=EMA(0.667*REF(主力,1)+0.333*主力,2);

STICKLINE(主力<散户,49,54,2,0),COLORGREEN;

A9:STICKLINE(主力>散户,49,54,2,0),COLORRED;

A10:STICKLINE((多方主力>空方主力) AND(MA22<= MA11) AND (主力>散户) ,56,61,3,0),COLORYELLOW;

STICKLINE((多方主力<空方主力) AND(MA22>MA11) AND (主力<散户) ,56,61,3,0),COLORCYAN;

A11:STICKLINE((多方主力>空方主力) AND (MA22<= MA11) AND (主力>散户) AND (DIFF>DEA) AND (K>D) AND (CBA1>CBA2) AND (指标营地1>指标营地2) AND (CLOSE>BBI) AND (MMS>MMM),63,68,3,0),COLORMAGENTA;

{微信公众号:尊重市场}

ABC1:=MA(CLOSE,10);

ABC2:=MA(CLOSE,55);

ABC3:=(REF(CLOSE,3)-CLOSE)/REF(CLOSE,3)*100>5;

ABC4:=FILTER(ABC3,10);

ABC5:=BARSLAST(ABC4);

ABC6:=REF(HIGH,ABC5+2);

ABC7:=REF(HIGH,ABC5+1);

ABC8:=REF(HIGH,ABC5);

ABC9:=MAX(ABC6,ABC7);

ABC10:=MAX(ABC9,ABC8);

ABC11:=(CLOSE-REF(CLOSE,1))/REF(CLOSE,1)*100>5;

ABC12:=ABC5<150;

ABC13:=(OPEN-ABC10)/ABC10*100<30;

ABC14:=(CLOSE-LLV(LOW,ABC5))/LLV(LOW,ABC5)*100<50;

ABC15:=(CLOSE-REF(OPEN,5))/REF(OPEN,5)*100<30;

ABC16:=VOL/MA(VOL,5)<3.5;

ABC17:=(CLOSE-REF(CLOSE,89))/REF(CLOSE,89)*100<80;

ABC18:=ABC11 AND ABC12 AND ABC13 AND ABC14 AND ABC15 AND ABC16 AND ABC17;

ABC19:=FILTER(ABC18,15);

ABC20:=(CLOSE-ABC2)/ABC2<0.1;

ABC21:=(CLOSE-ABC1)/ABC1<0.3;

ABC22:=(ABC20=1 AND ABC21=1)*0.2;

ABC23:=(ABC22=0 AND REF(ABC22,1)=0.2 AND REF(COUNT(ABC22=0.2,10)=10,1)=1)*(-0.1);

ABC24:=ABC23=(-0.1);

ABC25:=ABC19 OR ABC24;

ABC27:=VOL/REF(VOL,1)>1.2 AND CLOSE>OPEN OR (LOW>REF(HIGH,1) AND OPEN>CLOSE AND VOL/REF(VOL,1)>1.2);

{微信公众号:尊重市场}

ABC28:=IF(CODELIKE(3) OR (CODELIKE(4) AND DATE>=1200824),CLOSE>=ZTPRICE(REF(CLOSE,1),0.2) AND CLOSE=HIGH,CLOSE>=ZTPRICE(REF(CLOSE,1),0.1) AND CLOSE=HIGH);

共振:=ABC25 AND ABC27 AND ABC28 AND REF(NOT(ABC28),1) AND (多方主力>空方主力) AND (MA22<= MA11) AND (主力>散户) AND (DIFF>DEA) AND (K>D) AND (CBA1>CBA2) AND (指标营地1>指标营地2) AND (CLOSE>BBI) AND (MMS>MMM);

DRAWICON(共振,75,9);
```
2、钱袋子选股指标：

```
{微信公众号:尊重市场}

DIFF:=EMA(CLOSE,8)-EMA(CLOSE,13);

DEA:=EMA(DIFF,5);

尊重市场1:=(CLOSE-LLV(LOW,8))/(HHV(HIGH,8)-LLV(LOW,8))*100;

K:=SMA(尊重市场1,3,1);

D:=SMA(K,3,1);

LC:=REF(CLOSE,1);

CBA1:=(SMA(MAX(CLOSE-LC,0),5,1))/(SMA(ABS(CLOSE-LC),5,1))*100;

CBA2:=(SMA(MAX(CLOSE-LC,0),13,1))/(SMA(ABS(CLOSE-LC),13,1))*100;

{微信公众号:尊重市场}

尊重市场:=-(HHV(HIGH,13)-CLOSE)/(HHV(HIGH,13)-LLV(LOW,13))*100;

指标营地1:=SMA(尊重市场,3,1);

指标营地2:=SMA(指标营地1,3,1);

BBI:=(MA(CLOSE,3)+MA(CLOSE,6)+MA(CLOSE,12)+MA(CLOSE,24))/4;

{微信公众号:尊重市场}

MTM:=CLOSE-REF(CLOSE,1);

MMS:=100*EMA(EMA(MTM,5),3)/EMA(EMA(ABS(MTM),5),3);

MMM:=100*EMA(EMA(MTM,13),8)/EMA(EMA(ABS(MTM),13),8);

MAV:=(C*2+H+L)/4;

SK:=EMA(MAV,13)-EMA(MAV,34);

SD:=EMA(SK,5);

空方主力:=(-2*(SK-SD))*3.8,COLORGREEN;

多方主力:=(2*(SK-SD))*3.8,COLORRED;

MA11:=MA(CLOSE,5);

MA22:=REF(MA(CLOSE,5),1);

XYZ2:=IF(MONTH<12,1,1);

XYZ3:=(2*CLOSE+HIGH+LOW)/4;

XYZ4:=LLV(LOW,34);

XYZ5:=HHV(HIGH,34);

主力:=EMA((XYZ3-XYZ4)/(XYZ5-XYZ4)*100,13)*XYZ2;

散户:=EMA(0.667*REF(主力,1)+0.333*主力,2);

{微信公众号:尊重市场}

ABC1:=MA(CLOSE,10);

ABC2:=MA(CLOSE,55);

ABC3:=(REF(CLOSE,3)-CLOSE)/REF(CLOSE,3)*100>5;

ABC4:=FILTER(ABC3,10);

ABC5:=BARSLAST(ABC4);

ABC6:=REF(HIGH,ABC5+2);

ABC7:=REF(HIGH,ABC5+1);

ABC8:=REF(HIGH,ABC5);

ABC9:=MAX(ABC6,ABC7);

ABC10:=MAX(ABC9,ABC8);

ABC11:=(CLOSE-REF(CLOSE,1))/REF(CLOSE,1)*100>5;

ABC12:=ABC5<150;

ABC13:=(OPEN-ABC10)/ABC10*100<30;

ABC14:=(CLOSE-LLV(LOW,ABC5))/LLV(LOW,ABC5)*100<50;

ABC15:=(CLOSE-REF(OPEN,5))/REF(OPEN,5)*100<30;

ABC16:=VOL/MA(VOL,5)<3.5;

ABC17:=(CLOSE-REF(CLOSE,89))/REF(CLOSE,89)*100<80;

ABC18:=ABC11 AND ABC12 AND ABC13 AND ABC14 AND ABC15 AND ABC16 AND ABC17;

ABC19:=FILTER(ABC18,15);

ABC20:=(CLOSE-ABC2)/ABC2<0.1;

ABC21:=(CLOSE-ABC1)/ABC1<0.3;

ABC22:=(ABC20=1 AND ABC21=1)*0.2;

ABC23:=(ABC22=0 AND REF(ABC22,1)=0.2 AND REF(COUNT(ABC22=0.2,10)=10,1)=1)*(-0.1);

ABC24:=ABC23=(-0.1);

ABC25:=ABC19 OR ABC24;

ABC27:=VOL/REF(VOL,1)>1.2 AND CLOSE>OPEN OR (LOW>REF(HIGH,1) AND OPEN>CLOSE AND VOL/REF(VOL,1)>1.2);

{微信公众号:尊重市场}

ABC28:=IF(CODELIKE(3) OR (CODELIKE(4) AND DATE>=1200824),CLOSE>=ZTPRICE(REF(CLOSE,1),0.2) AND CLOSE=HIGH,CLOSE>=ZTPRICE(REF(CLOSE,1),0.1) AND CLOSE=HIGH);

共振选:ABC25 AND ABC27 AND ABC28 AND REF(NOT(ABC28),1) AND (多方主力>空方主力) AND (MA22<= MA11) AND (主力>散户) AND (DIFF>DEA) AND (K>D) AND (CBA1>CBA2) AND (指标营地1>指标营地2) AND (CLOSE>BBI) AND (MMS>MMM);
```
python源代码解析

```

```
### 5小果波段掘金
趋势轮动模型
![输入图片说明](image5.1.png)
![输入图片说明](image5.2.png)
![输入图片说明](image5.3.png)

通达信源代码

```
MA3:MA(C,3);
MA5:MA(C,5),COLORYELLOW;
MA10:MA(C,10);
MA15:MA(C,15),COLORWHITE;
MA20:MA(C,20),COLORGREEN,POINTDOT;
MA30:MA(C,30),COLORRED,POINTDOT;
A1:=IF(C>=MA3,1,-1);
A2:=IF(C>=MA5,1,-1);
A3:=IF(C>=MA10,1,-1);
A4:=IF(MA3>=REF(MA3,1),1,-1);
A5:=IF(MA5>=REF(MA5,1),1,-1);
A6:=IF(MA10>=REF(MA10,1),1,-1);
QUSHIX:=(A1+A2+A3+A4+A5+A6)/6*100,COLORCYAN,LINETHICK3;
X1:=(C+L+H)/3;
X2:=EMA(X1,3);
X3:=EMA(X2,5);
DRAWTEXT(CROSS(X2,X3),L*0.98,'B');
DRAWTEXT(CROSS(X3,X2),H*1.02,'S');
STICKLINE(X2>=X3,LOW,HIGH,0,0),COLORRED;STICKLINE(X2>=X3,CLOSE,OPEN,3,1),COLORRED;
STICKLINE(X2<X3,LOW,HIGH,0,0),COLORGREEN;STICKLINE(X2<X3,CLOSE,OPEN,3,1),COLORGREEN;
STICKLINE(CROSS(X2,X3),OPEN,CLOSE,3,0),COLORYELLOW;
STICKLINE(CROSS(X3,X2),OPEN,CLOSE,3,0),COLORBLUE;DRAWTEXT(QUSHIX>=100 AND MA3>REF(MA3,1) AND (CLOSE-OPEN)/OPEN*100>5 AND CLOSE>MA3,L*0.99,'★'),COLORMAGENTA;
```
python源代码

```
from xg_tdx_func.xg_tdx_func import *
from trader_tool.unification_data import unification_data
class small_fruit_band_gold_mining:
    def __init__(self,df):
        '''
        小果波段掘金
        '''
        self.df=df
    def small_fruit_band_gold_mining(self):
        '''
        输出MA3:收盘价的3日简单移动平均
        输出MA5:收盘价的5日简单移动平均,画黄色
        输出MA10:收盘价的10日简单移动平均
        输出MA15:收盘价的15日简单移动平均,画白色
        输出MA20:收盘价的20日简单移动平均,画绿色,POINTDOT
        输出MA30:收盘价的30日简单移动平均,画红色,POINTDOT
        A1赋值:如果收盘价>=MA3,返回1,否则返回-1
        A2赋值:如果收盘价>=MA5,返回1,否则返回-1
        A3赋值:如果收盘价>=MA10,返回1,否则返回-1
        A4赋值:如果MA3>=1日前的MA3,返回1,否则返回-1
        A5赋值:如果MA5>=1日前的MA5,返回1,否则返回-1
        A6赋值:如果MA10>=1日前的MA10,返回1,否则返回-1
        QUSHIX赋值:(A1+A2+A3+A4+A5+A6)/6*100,画青色,线宽为3
        X1赋值:(收盘价+最低价+最高价)/3
        X2赋值:X1的3日指数移动平均
        X3赋值:X2的5日指数移动平均
        当满足条件X2上穿X3时,在最低价*0.98位置书写文字
        当满足条件X3上穿X2时,在最高价*1.02位置书写文字
        当满足条件X2>=X3时,在最低价和最高价位置之间画柱状线,宽度为0,0不为0则画空心柱.,画红色
        当满足条件X2<X3时,在最低价和最高价位置之间画柱状线,宽度为0,0不为0则画空心柱.,画绿色
        当满足条件X2上穿X3时,在开盘价和收盘价位置之间画柱状线,宽度为3,0不为0则画空心柱.,画黄色
        当满足条件X3上穿X2时,在开盘价和收盘价位置之间画柱状线,宽度为3,0不为0则画空心柱.,画蓝色
        当满足条件QUSHIX>=100ANDMA3>1日前的MA3AND(收盘价-开盘价)/开盘价*100>5ANDCLOSE>MA3时,在最低价*0.99位置书写文字,画洋红色
        '''
        df=self.df
        CLOSE=df['close']
        C=df['close']
        LOW=df['low']
        L=df['low']
        HIGH=df['high']
        H=df['high']
        OPEN=df['open']
        O=df['open']
        volume=df['volume']
        V=df['volume']
        MA3=MA(C,3)
        MA5=MA(C,5)
        MA10=MA(C,10)
        MA15=MA(C,15)
        MA20=MA(C,20)
        MA30=MA(C,30)
        A1=IF(C>=MA3,1,-1)
        A2=IF(C>=MA5,1,-1)
        A3=IF(C>=MA10,1,-1)
        A4=IF(MA3>=REF(MA3,1),1,-1)
        A5=IF(MA5>=REF(MA5,1),1,-1)
        A6=IF(MA10>=REF(MA10,1),1,-1)
        QUSHIX=(A1+A2+A3+A4+A5+A6)/6*100
        X1=(C+L+H)/3
        X2=EMA(X1,3)
        X3=EMA(X2,5)
        #DRAWTEXT(CROSS(X2,X3),L*0.98,'B');
        df['B']=CROSS(X2,X3)
        #DRAWTEXT(CROSS(X3,X2),H*1.02,'S');
        df['S']=CROSS(X3,X2)
        #STICKLINE(X2>=X3,LOW,HIGH,0,0),COLORRED;
        #STICKLINE(X2>=X3,CLOSE,OPEN,3,1),COLORRED;
        df['红色']=X2>=X3
        #STICKLINE(X2<X3,LOW,HIGH,0,0),COLORGREEN;
        #STICKLINE(X2<X3,CLOSE,OPEN,3,1),COLORGREEN;
        df['绿色']=X2<X3
        #STICKLINE(CROSS(X2,X3),OPEN,CLOSE,3,0),COLORYELLOW;
        df['黄色']=CROSS(X2,X3)
        #STICKLINE(CROSS(X3,X2),OPEN,CLOSE,3,0),COLORBLUE;
        df['蓝色']=CROSS(X3,X2)
        #DRAWTEXT(QUSHIX>=100 AND MA3>REF(MA3,1) AND (CLOSE-OPEN)/OPEN*100>5 AND CLOSE>MA3,L*0.99,'★'),COLORMAGENTA
        df['星']=AND(AND(AND(QUSHIX>=100,MA3>REF(MA3,1)),(CLOSE-OPEN)/OPEN*100>5),CLOSE>MA3)
        return df
if __name__=='__main__':
    data=unification_data(trader_tool='ths')
    data=data.get_unification_data()
    df=data.get_hist_data_em(stock='513100')
    modes=small_fruit_band_gold_mining(df=df)
    result=modes.small_fruit_band_gold_mining()
    print(result)
    result.to_excel(r'数据.xlsx')
```
解析的结果

```
操作方式,登录qmt,选择行情加交易选,择极简模式
作者:小果
作者微信:15117320079,开实盘qmt可以联系我,开户也可以
作者微信公众号:数据分析与运用
公众号链接:https://mp.weixin.qq.com/s/rxGJpZYxdUIHitjvI-US1A 
作者知识星球:金融量化交易研究院  https://t.zsxq.com/19VzjjXNi
通达信数据连接成功
通达信数据连接成功
通达信数据连接成功
            date   open  close   high    low   volume          成交额    振幅   涨跌幅    涨跌额   换手率      B      S     红色     绿色     黄色     蓝
色      星
0     2013-05-15  0.198  0.199  0.200  0.198   877116   87379278.0  1.00 -0.50 -0.001  0.93  False  False   True  False  False  False  False
1     2013-05-16  0.199  0.200  0.200  0.199   265570   26501420.0  0.50  0.50  0.001  0.28   True  False   True  False   True  False  False
2     2013-05-17  0.200  0.199  0.200  0.199    36379    3630062.0  0.50 -0.50 -0.001  0.04  False  False   True  False  False  False  False
3     2013-05-20  0.200  0.199  0.201  0.199    80490    8033039.0  1.01  0.00  0.000  0.09  False  False   True  False  False  False  False
4     2013-05-21  0.199  0.199  0.200  0.199   161124   16023195.0  0.50  0.00  0.000  0.17  False  False   True  False  False  False  False
...          ...    ...    ...    ...    ...      ...          ...   ...   ...    ...   ...    ...    ...    ...    ...    ...    ...    ...
2755  2024-09-09  1.349  1.364  1.366  1.343  5481686  741657926.0  1.68 -0.58 -0.008  5.80  False  False  False   True  False  False  False
2756  2024-09-10  1.375  1.368  1.377  1.367  4460372  612238602.0  0.73  0.29  0.004  4.72  False  False  False   True  False  False  False
2757  2024-09-11  1.381  1.377  1.382  1.369  4461679  613644268.0  0.95  0.66  0.009  4.72  False  False  False   True  False  False  False
2758  2024-09-12  1.414  1.421  1.423  1.412  6535594  925906001.0  0.80  3.20  0.044  6.92   True  False   True  False   True  False  False
2759  2024-09-13  1.432  1.435  1.437  1.429  4521259  647791882.0  0.56  0.99  0.014  4.79  False  False   True  False  False  False  False

[2760 rows x 18 columns]
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
